package com.example.marketmaker.referenceprices;

import java.math.BigDecimal;
import java.util.Queue;
import com.example.marketmaker.referenceprices.impl.PriceUpdate;

/**
 * Callback interface for {@link ReferencePriceSource}
 */
public interface ReferencePriceSourceListener
{

    /**
     * Called when a price has changed.
     *
     * @param securityId security identifier
     * @param price      reference price
     */
    void referencePriceChanged(int securityId, BigDecimal price);

    /**
     * Called to accept subscription from the {@code ReferencePriceSource}
     *
     * @param publishQueue the queue to which publish the price updates
     */
    void accept(Queue<PriceUpdate> publishQueue);
}
