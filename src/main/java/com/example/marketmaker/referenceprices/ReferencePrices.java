package com.example.marketmaker.referenceprices;

import java.math.BigDecimal;
import javax.annotation.Nonnull;
import com.example.marketmaker.exceptions.ReferencePriceMissingException;

/**
 * Wrapper for the reference price store. This allows the underlying storage medium (e.g. data structure, file, etc.) to be changed easily.
 */
public interface ReferencePrices
{
    /**
     * Gets the price.
     *
     * @param securityId the identifier of the underlying asset.
     * @return the reference price.
     * @throws ReferencePriceMissingException if the securityId cannot be found.
     */
    @Nonnull
    BigDecimal getPrice(int securityId) throws ReferencePriceMissingException;

    /**
     * Updates the price.
     *
     * @param securityId the identifier of the underlying asset.
     * @param newPrice   the updated price.
     */
    void updatePrice(int securityId, @Nonnull BigDecimal newPrice);

    /**
     * Returns the number of security IDs.
     *
     * @return the number of security IDs.
     */
    int size();
}
