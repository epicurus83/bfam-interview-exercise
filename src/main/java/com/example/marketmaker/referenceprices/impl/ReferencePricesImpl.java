package com.example.marketmaker.referenceprices.impl;

import static java.util.Objects.nonNull;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicReferenceArray;
import javax.annotation.Nonnull;
import com.example.marketmaker.exceptions.ReferencePriceMissingException;
import com.example.marketmaker.referenceprices.ReferencePrices;

public class ReferencePricesImpl implements ReferencePrices
{
    /**
     * This might look a little unconventional but I thought this was better than a HashMap<Integer, BigDecimal> for two reasons:
     * 1. As securityId is a primitive int value, calling ReferencePriceSource.get(int) would involve autoboxing the int when calling get on the HashMap. Not a very expensive operation
     * but as quotes are requested at a high frequency.
     * 2. Saves space as an array will take up less memory than a hashmap (although wrapped in AtomicReferenceArray this may not be the case).
     */
    private final AtomicReferenceArray<BigDecimal> referencePrices;

    private ReferencePricesImpl(AtomicReferenceArray<BigDecimal> referencePrices)
    {
        this.referencePrices = referencePrices;
    }

    @Nonnull
    @Override
    public BigDecimal getPrice(int securityId) throws ReferencePriceMissingException
    {
        BigDecimal price = referencePrices.get(securityId);
        if (nonNull(price))
        {
            return price;
        }
        throw new ReferencePriceMissingException("Security ID(" + securityId + ") is not recognized.");
    }

    @Override
    public void updatePrice(int securityId, @Nonnull BigDecimal newPrice)
    {
        referencePrices.set(securityId, newPrice);
    }

    @Override
    public int size()
    {
        return referencePrices.length();
    }
}
