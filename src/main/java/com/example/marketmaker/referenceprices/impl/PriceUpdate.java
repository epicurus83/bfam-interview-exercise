package com.example.marketmaker.referenceprices.impl;

import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PriceUpdate
{
    private final int securityId;
    private final BigDecimal price;
}
