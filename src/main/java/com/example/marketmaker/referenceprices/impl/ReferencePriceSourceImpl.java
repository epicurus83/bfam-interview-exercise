package com.example.marketmaker.referenceprices.impl;

import static java.util.Objects.isNull;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.annotation.Nonnull;
import com.example.marketmaker.exceptions.ReferencePriceMissingException;
import com.example.marketmaker.referenceprices.ReferencePriceSource;
import com.example.marketmaker.referenceprices.ReferencePriceSourceListener;
import com.example.marketmaker.referenceprices.ReferencePrices;

public class ReferencePriceSourceImpl implements ReferencePriceSource
{
    private static ReferencePriceSourceImpl INSTANCE;

    private final ReferencePrices referencePrices;
    private final Queue<PriceUpdate> priceUpdates;
    private final ExecutorService executorService;

    /**
     * In an actual system these would likely be all sourced from a third party system (maybe the exchange?).
     * Likely the ReferencePriceSourceListener would be connected to this third party system and it would load up all the reference prices on system startup.
     * However, I will assume that the initial reference prices can be passed in the constructor.
     */
    private ReferencePriceSourceImpl(ReferencePrices referencePrices, Queue<PriceUpdate> priceUpdates)
    {
        this.referencePrices = referencePrices;
        this.priceUpdates = priceUpdates;
        executorService = Executors.newSingleThreadExecutor();
        pollUpdate();
    }

    public static ReferencePriceSourceImpl getInstance(ReferencePrices referencePrices, Queue<PriceUpdate> priceUpdates)
    {
        if (Objects.isNull(INSTANCE))
        {
            INSTANCE = new ReferencePriceSourceImpl(referencePrices, priceUpdates);
        }
        return INSTANCE;
    }

    @Override
    public void subscribe(@Nonnull ReferencePriceSourceListener listener)
    {
        listener.accept(priceUpdates);
    }

    @Override
    public BigDecimal get(int securityId) throws ReferencePriceMissingException
    {
        if (securityId < 0 || securityId >= referencePrices.size())
        {
            throw new ReferencePriceMissingException("Security ID(" + securityId + ") cannot be found.");
        }
        return referencePrices.getPrice(securityId);
    }

    public void pollUpdate()
    {
        executorService.execute(() ->
        {
            while (true)
            {
                PriceUpdate priceUpdate = priceUpdates.poll();
                if (isNull(priceUpdate))
                {
                    continue;
                }
                referencePrices.updatePrice(priceUpdate.getSecurityId(), priceUpdate.getPrice());
            }
        });
    }
}
