package com.example.marketmaker.referenceprices.impl;

import java.math.BigDecimal;
import java.util.Queue;
import com.example.marketmaker.referenceprices.ReferencePriceSourceListener;

public class ReferencePriceSourceListenerImpl implements ReferencePriceSourceListener
{
    private Queue<PriceUpdate> publishQueue;

    @Override
    public void referencePriceChanged(int securityId, BigDecimal price)
    {
        publishQueue.offer(new PriceUpdate(securityId, price));
    }

    @Override
    public void accept(Queue<PriceUpdate> publishQueue)
    {
        this.publishQueue = publishQueue;
    }
}
