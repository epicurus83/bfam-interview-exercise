package com.example.marketmaker.referenceprices;

import java.math.BigDecimal;
import javax.annotation.Nonnull;
import com.example.marketmaker.exceptions.ReferencePriceMissingException;

/**
 * Source for reference prices.
 * I changed get(int securityId) to return a BigDecimal as I think it is more accurate.
 */
public interface ReferencePriceSource
{
    /**
     * Subscribe to changes to reference prices.
     *
     * @param listener callback interface for changes.
     */
    void subscribe(@Nonnull ReferencePriceSourceListener listener);

    /**
     * Gets the reference price.
     *
     * @param securityId the identifier of the underlying asset.
     * @return the reference price.
     * @throws ReferencePriceMissingException if the securityId cannot be found.
     */
    BigDecimal get(int securityId) throws ReferencePriceMissingException;
}
