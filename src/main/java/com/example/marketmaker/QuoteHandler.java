package com.example.marketmaker;

import static java.util.Objects.nonNull;

import java.math.BigDecimal;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import com.example.marketmaker.quotecalculationengine.ConcurrentQuoteHandlingEngine;
import com.example.marketmaker.quotecalculationengine.QuoteCalculationEngineFactory;
import com.example.marketmaker.quotecalculationengine.impl.ConcurrentQuoteHandlingEngineImpl;
import com.example.marketmaker.quoterequest.QuoteRequest;
import com.example.marketmaker.referenceprices.ReferencePriceSource;
import com.example.marketmaker.referenceprices.ReferencePriceSourceListener;
import com.example.marketmaker.referenceprices.ReferencePrices;
import com.example.marketmaker.referenceprices.impl.PriceUpdate;
import com.example.marketmaker.referenceprices.impl.ReferencePriceSourceImpl;

public class QuoteHandler
{
    private final ConcurrentQuoteHandlingEngine concurrentQuoteHandlingEngine;
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private final BlockingQueue<String> calculatedQuotes = new LinkedBlockingQueue<>();

    /**
     * This number of engines would probably not be passed in as a constructor in the actual system, it would be configured in file.
     *
     * @param engineType      the QuoteCalculationEngine implementation.
     * @param numOfEngines    the number of concurrent engines to be run.
     * @param referencePrices the reference prices used in the quote calculation.
     * @param priceUpdates    the queue used by the ReferencePriceSourceListener.
     * @param listener        the api which is used to update reference prices.
     */
    public QuoteHandler(QuoteCalculationEngineFactory.TYPE engineType, int numOfEngines, ReferencePrices referencePrices, Queue<PriceUpdate> priceUpdates, ReferencePriceSourceListener listener)
    {
        ReferencePriceSource referencePriceSource = ReferencePriceSourceImpl.getInstance(referencePrices, priceUpdates);
        referencePriceSource.subscribe(listener);
        concurrentQuoteHandlingEngine = new ConcurrentQuoteHandlingEngineImpl(referencePriceSource, engineType, numOfEngines);
        pollCalculateQuotes();
    }

    private void pollCalculateQuotes()
    {
        executorService.execute(() ->
        {
            while (true)
            {
                BigDecimal quote = concurrentQuoteHandlingEngine.pollQuote();

                if (nonNull(quote))
                {
                    calculatedQuotes.offer(quote.toString());
                }
            }
        });
    }

    /**
     * This is the public API for submitting a quote request.
     *
     * @param quoteRequestRaw the string quote request (e.g. '100 BUY 123').
     */
    public void submitQuoteRequest(String quoteRequestRaw)
    {
        try
        {
            QuoteRequest quoteRequest = MessageParser.parseQuoteRequest(quoteRequestRaw);
            concurrentQuoteHandlingEngine.calculate(quoteRequest);
        }
        catch (Exception e)
        {
            System.out.println("Quote request '" + quoteRequestRaw + "' cannot be processed: " + e.toString());
        }
    }

    /**
     * This is the public API for getting back a calculated quote.
     * The actual server would keep track of the session of the client requesting the quote. Then the quote could be returned to the correct client. We will assume only 1 client uses the system for simplicity.
     *
     * @return the calculated quote (e.g. '112').
     */
    public String getQuote()
    {
        return calculatedQuotes.poll();
    }
}
