package com.example.marketmaker.exceptions;

public class ReferencePriceMissingException extends Exception
{
    public ReferencePriceMissingException(String message)
    {
        super(message);
    }
}
