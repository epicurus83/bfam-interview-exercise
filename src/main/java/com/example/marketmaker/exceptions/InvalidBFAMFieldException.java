package com.example.marketmaker.exceptions;

public class InvalidBFAMFieldException extends Exception
{
    public InvalidBFAMFieldException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
