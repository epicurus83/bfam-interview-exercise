package com.example.marketmaker.quoterequest;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import com.example.marketmaker.exceptions.InvalidBFAMFieldException;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;

public class QuoteRequestFactory
{
    /**
     * Private constructor as this class should not be instantiated.
     */
    private QuoteRequestFactory()
    {
    }

    /**
     * Maps the FIELD_NAME to the exact field type (e.g. FieldFinancial, FieldString)
     */
    private static final ImmutableMap<FIELD_NAME, Class<? extends FieldAbstract<?>>> FIELD_NAME_TYPE_MAP = new ImmutableMap.Builder<FIELD_NAME, Class<? extends FieldAbstract<?>>>()
        .put(FIELD_NAME.SECURITY_ID, FieldString.class)
        .put(FIELD_NAME.SIDE, FieldSide.class)
        .put(FIELD_NAME.QUANTITY, FieldFinancial.class)
        .build();

    /**
     * The public API for getting a QuoteRequest object from a map of field values.
     *
     * @param fieldMapRaw The field to value map. The field name and field values should all be Strings.
     * @return The quote request object.
     * @throws InvalidBFAMFieldException If any of the fields fails validation.
     */
    @Nonnull
    public static QuoteRequest getQuoteRequest(@Nullable Map<String, String> fieldMapRaw) throws InvalidBFAMFieldException
    {
        Map<FIELD_NAME, FieldAbstract<?>> fieldMap = tryBuildFieldMap(fieldMapRaw);
        return new QuoteRequest(fieldMap);
    }

    @Nonnull
    private static Map<FIELD_NAME, FieldAbstract<?>> tryBuildFieldMap(@Nullable Map<String, String> fieldMapRaw) throws InvalidBFAMFieldException
    {
        try
        {
            return buildFieldMap(fieldMapRaw);
        }
        catch (Exception e)
        {
            throw new InvalidBFAMFieldException("Cannot instantiate QuoteRequest from parameters(" + fieldMapRaw + ").", e);
        }
    }

    @Nonnull
    private static Map<FIELD_NAME, FieldAbstract<?>> buildFieldMap(@Nullable Map<String, String> fieldMapRaw)
    {
        Preconditions.checkNotNull(fieldMapRaw);

        Map<FIELD_NAME, FieldAbstract<?>> fieldMap = new HashMap<>();

        for (Map.Entry<String, String> fieldMapRawEntry : fieldMapRaw.entrySet())
        {
            FIELD_NAME fieldName = FIELD_NAME.valueOf(fieldMapRawEntry.getKey());
            FieldAbstract<?> fieldValue = getFieldValue(fieldName, fieldMapRawEntry.getValue());
            FieldValidator.validate(fieldName, fieldValue);
            fieldMap.put(fieldName, fieldValue);
        }
        return fieldMap;
    }

    @Nonnull
    private static FieldAbstract<?> getFieldValue(@Nonnull FIELD_NAME fieldName, @Nullable String value)
    {
        Preconditions.checkNotNull(value);
        if (Objects.equals(FIELD_NAME_TYPE_MAP.get(fieldName), FieldString.class))
        {
            return new FieldString(value);
        }
        if (Objects.equals(FIELD_NAME_TYPE_MAP.get(fieldName), FieldSide.class))
        {
            return new FieldSide(QuoteRequest.SIDE.valueOf(value));
        }
        return new FieldFinancial(new BigDecimal(value));
    }
}
