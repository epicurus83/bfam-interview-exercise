package com.example.marketmaker.quoterequest;

/**
 * The names of the Quote Request fields.
 */
public enum FIELD_NAME
{
    SECURITY_ID, SIDE, QUANTITY
}
