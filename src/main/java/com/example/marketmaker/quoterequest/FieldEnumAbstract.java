/*************************************************************************
 * ULLINK CONFIDENTIAL INFORMATION
 * _______________________________
 *
 * All Rights Reserved.
 *
 * NOTICE: This file and its content are the property of Ullink. The
 * information included has been classified as Confidential and may
 * not be copied, modified, distributed, or otherwise disseminated, in
 * whole or part, without the express written permission of Ullink.
 ************************************************************************/
package com.example.marketmaker.quoterequest;

public abstract class FieldEnumAbstract<T> extends FieldAbstract<T>
{
    FieldEnumAbstract(T value)
    {
        super(value);
    }

    public boolean compareEnum(T enumValue)
    {
        return getValue() == enumValue;
    }
}
