package com.example.marketmaker.quoterequest;

import lombok.EqualsAndHashCode;

/**
 * A common parent class for the fields in the Quote request.
 * Usually composition is preferable to inheritance but in this instance I think its better as the fields have a 'is a' relationship with FieldAbstract and this is an easier way to reuse code.
 * The naming looks peculiar (e.g. FieldAbstract instead of AbstractField). This was so all the different Field Implementations would be adjacent in the IDE (alphabetical ordering).
 *
 * @param <T> The type of the field (e.g. String or BigDecimal).
 */
@EqualsAndHashCode
public abstract class FieldAbstract<T>
{
    private final T value;

    FieldAbstract(T value)
    {
        this.value = value;
    }

    T getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return value.toString();
    }
}
