package com.example.marketmaker.quoterequest;

import java.math.BigDecimal;

public class FieldFinancial extends FieldAbstract<BigDecimal>
{
    FieldFinancial(BigDecimal value)
    {
        super(value);
    }
}
