package com.example.marketmaker.quoterequest;

import java.util.Map;
import javax.annotation.Nonnull;
import com.google.common.base.Objects;

public class QuoteRequest
{
    public enum SIDE
    {
        BUY, SELL
    }

    /**
     * A more extensible design than defining all the fields as member variables of the class.
     * It also means equality checking can be delegated to the HashMap class.
     */
    private final Map<FIELD_NAME, FieldAbstract<?>> fieldMap;

    /**
     * Should not be instantiated directly.
     *
     * @param fieldMap field to field value map.
     * @see QuoteRequestFactory#getQuoteRequest(Map)
     */
    QuoteRequest(@Nonnull Map<FIELD_NAME, FieldAbstract<?>> fieldMap)
    {
        this.fieldMap = fieldMap;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        QuoteRequest that = (QuoteRequest) o;
        return Objects.equal(fieldMap, that.fieldMap);
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(fieldMap);
    }

    public FieldAbstract<?> getField(FIELD_NAME fieldName)
    {
        return fieldMap.get(fieldName);
    }
}
