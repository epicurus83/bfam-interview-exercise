package com.example.marketmaker.quoterequest;

import static java.util.Objects.nonNull;

import java.math.BigDecimal;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.google.common.collect.ImmutableMap;

/**
 * The validators all return true. This might be a bit misleading. If the value does not pass the validation then an exception is thrown.
 */
class FieldValidator
{
    /**
     * Private constructor as this class should not be instantiated.
     */
    private FieldValidator()
    {
    }

    /**
     * Used to validate Security ID which is just an integer identifying the Security
     */
    private static final Matcher POSITIVE_INTEGER_MATCHER = Pattern.compile("^[0-9]+$").matcher("");

    /**
     * Used to validate quantity which shouldn't be negative and shouldn't contain a fractional part
     */
    private static final Predicate<FieldFinancial> quantityFinancialFieldValidator = fieldFinancial ->
    {
        BigDecimal value = fieldFinancial.getValue();
        if (value.compareTo(BigDecimal.ZERO) < 0)
        {
            throw new IllegalArgumentException("Quantity cannot be less than '0'");
        }
        if (BigDecimal.ZERO.compareTo(value.remainder(BigDecimal.ONE)) != 0)
        {
            throw new IllegalArgumentException("Financial value cannot contain fractional part (e.g. 100.1)");
        }
        return true;
    };

    private static final ImmutableMap<FIELD_NAME, Predicate<FieldFinancial>> FINANCIAL_FIELD_VALIDATOR = new ImmutableMap.Builder<FIELD_NAME, Predicate<FieldFinancial>>()
        .put(FIELD_NAME.QUANTITY, quantityFinancialFieldValidator)
        .build();

    private static final ImmutableMap<FIELD_NAME, Predicate<FieldString>> STRING_FIELD_VALIDATOR = new ImmutableMap.Builder<FIELD_NAME, Predicate<FieldString>>()
        .put(FIELD_NAME.SECURITY_ID, value ->
        {
            POSITIVE_INTEGER_MATCHER.reset(value.getValue());
            if (!POSITIVE_INTEGER_MATCHER.find())
            {
                throw new IllegalArgumentException(FIELD_NAME.SECURITY_ID.toString() + " field must a positive integer");
            }
            return true;
        })
        .put(FIELD_NAME.SIDE, value ->
        {
            QuoteRequest.SIDE.valueOf(value.getValue());
            return true;
        })
        .build();

    /**
     * The validator Predicate is retrieved from the Immutable Maps. If no Predicate is found for a field, it is assumed that the field requires no validation.
     *
     * @param fieldName  name of the field to be validated.
     * @param fieldValue the value.
     */
    static void validate(FIELD_NAME fieldName, FieldAbstract<?> fieldValue)
    {
        if (fieldValue instanceof FieldFinancial)
        {
            Predicate<FieldFinancial> validator = FINANCIAL_FIELD_VALIDATOR.get(fieldName);
            if (nonNull(validator))
            {
                validator.test((FieldFinancial) fieldValue);
            }
        }
        if (fieldValue instanceof FieldString)
        {
            Predicate<FieldString> validator = STRING_FIELD_VALIDATOR.get(fieldName);
            if (nonNull(validator))
            {
                validator.test((FieldString) fieldValue);
            }
        }
    }
}
