package com.example.marketmaker.quotecalculationengine.impl;

import java.math.BigDecimal;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import com.example.marketmaker.exceptions.ReferencePriceMissingException;
import com.example.marketmaker.quotecalculationengine.ConcurrentQuoteHandlingEngine;
import com.example.marketmaker.quotecalculationengine.QuoteCalculationEngineFactory;
import com.example.marketmaker.quoterequest.FIELD_NAME;
import com.example.marketmaker.quoterequest.FieldSide;
import com.example.marketmaker.quoterequest.QuoteRequest;
import com.example.marketmaker.referenceprices.ReferencePriceSource;

public class ConcurrentQuoteHandlingEngineImpl extends ConcurrentQuoteHandlingEngine
{
    private final ExecutorService executorService;
    private final BlockingQueue<BigDecimal> quotes = new LinkedBlockingQueue<>();

    public ConcurrentQuoteHandlingEngineImpl(ReferencePriceSource referencePriceSource, QuoteCalculationEngineFactory.TYPE engineType, int numOfEngines)
    {
        super(referencePriceSource, engineType, numOfEngines);
        executorService = Executors.newFixedThreadPool(numOfEngines);
    }

    @Override
    public void calculate(QuoteRequest quoteRequest)
    {
        executorService.execute(() ->
        {
            int securityId = Integer.parseInt((quoteRequest.getField(FIELD_NAME.SECURITY_ID)).toString());
            FieldSide side = (FieldSide) quoteRequest.getField(FIELD_NAME.SIDE);
            boolean buy = side.compareEnum(QuoteRequest.SIDE.BUY);
            int quantity = Integer.parseInt(quoteRequest.getField(FIELD_NAME.QUANTITY).toString());
            try
            {
                quotes.put(quoteCalculationEngine.calculateQuotePrice(securityId, referencePriceSource.get(securityId), buy, quantity));
            }
            catch (InterruptedException | ReferencePriceMissingException e)
            {
                System.out.println("Quote couldn't be calculated because: " + e.getMessage());
            }
        });
    }

    @Override
    public BigDecimal pollQuote()
    {
        return quotes.poll();
    }
}
