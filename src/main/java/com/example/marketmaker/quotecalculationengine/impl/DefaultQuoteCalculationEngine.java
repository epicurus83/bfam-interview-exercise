package com.example.marketmaker.quotecalculationengine.impl;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;
import com.example.marketmaker.exceptions.ReferencePriceMissingException;
import com.example.marketmaker.quotecalculationengine.QuoteCalculationEngine;

/**
 * This class doesn't perform the calculation as it is very complicated.
 * Instead it returns the reference price and sets a thread delay of 3 seconds (to simulate a slow running time).
 * Then this can be used to test that the system can handle high volume of requests.
 */
public class DefaultQuoteCalculationEngine implements QuoteCalculationEngine
{
    @Nonnull
    @Override
    public BigDecimal calculateQuotePrice(int securityId, BigDecimal referencePrice, boolean buy, int quantity) throws ReferencePriceMissingException
    {
        try
        {
            TimeUnit.SECONDS.sleep(3);
        }
        catch (InterruptedException e)
        {
            // I'm not handling because an actual implementation wouldn't be calling sleep().
        }
        return referencePrice;
    }
}
