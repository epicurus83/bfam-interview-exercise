package com.example.marketmaker.quotecalculationengine;

import java.math.BigDecimal;
import com.example.marketmaker.exceptions.ReferencePriceMissingException;
import com.example.marketmaker.quoterequest.QuoteRequest;
import com.example.marketmaker.referenceprices.ReferencePriceSource;

public abstract class ConcurrentQuoteHandlingEngine
{
    protected final ReferencePriceSource referencePriceSource;
    protected final QuoteCalculationEngine quoteCalculationEngine;
    protected final int numOfEngines;

    public ConcurrentQuoteHandlingEngine(ReferencePriceSource referencePriceSource, QuoteCalculationEngineFactory.TYPE engineType, int numOfEngines)
    {
        this.referencePriceSource = referencePriceSource;
        this.quoteCalculationEngine = QuoteCalculationEngineFactory.get(engineType);
        this.numOfEngines = numOfEngines;
    }

    public abstract void calculate(QuoteRequest quoteRequest) throws ReferencePriceMissingException;

    public abstract BigDecimal pollQuote();
}
