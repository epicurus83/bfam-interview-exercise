package com.example.marketmaker.quotecalculationengine;

import java.math.BigDecimal;
import javax.annotation.Nonnull;
import com.example.marketmaker.exceptions.ReferencePriceMissingException;

/**
 * Provides access to to libraries for pricing of quote requests.
 */
public interface QuoteCalculationEngine
{

    /**
     * Calculate the appropriate price for a quote request on a security.
     * I changed the return value/referencePrice/quantity to BigDecimal because I think its more accurate.
     * This may take a long time to execute.
     *
     * @param securityId     security identifier
     * @param referencePrice reference price to calculate theory price from (e.g. underlying security's price)
     * @param buy            {@code true} if buy, otherwise sell
     * @param quantity       quantity for quote
     * @return calculated quote price
     */
    @Nonnull
    BigDecimal calculateQuotePrice(int securityId, BigDecimal referencePrice, boolean buy, int quantity) throws ReferencePriceMissingException;
}
