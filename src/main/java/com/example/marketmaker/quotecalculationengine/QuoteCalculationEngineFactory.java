package com.example.marketmaker.quotecalculationengine;

import javax.annotation.Nonnull;
import com.example.marketmaker.quotecalculationengine.impl.DefaultQuoteCalculationEngine;

public class QuoteCalculationEngineFactory
{
    public enum TYPE
    {
        DEFAULT
    }

    /**
     * Private constructor as this class should not be instantiated.
     */
    private QuoteCalculationEngineFactory()
    {
    }

    @Nonnull
    public static QuoteCalculationEngine get(TYPE type)
    {
        if (type == TYPE.DEFAULT)
        {
            return new DefaultQuoteCalculationEngine();
        }
        return new DefaultQuoteCalculationEngine();
    }
}
