/*************************************************************************
 * ULLINK CONFIDENTIAL INFORMATION
 * _______________________________
 *
 * All Rights Reserved.
 *
 * NOTICE: This file and its content are the property of Ullink. The
 * information included has been classified as Confidential and may
 * not be copied, modified, distributed, or otherwise disseminated, in
 * whole or part, without the express written permission of Ullink.
 ************************************************************************/
package com.example.marketmaker;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.example.marketmaker.exceptions.InvalidBFAMFieldException;
import com.example.marketmaker.exceptions.InvalidBFAMMessageException;
import com.example.marketmaker.quoterequest.FIELD_NAME;
import com.example.marketmaker.quoterequest.QuoteRequest;
import com.example.marketmaker.quoterequest.QuoteRequestFactory;
import com.google.common.base.Preconditions;

public class MessageParser
{
    /**
     * Regex for the Quote Request (e.g. '100 BUY 123').
     */
    private final static Matcher QUOTE_REQUEST_MESSAGE = Pattern.compile("^([0-9]+) (BUY|SELL) ([0-9]+)$").matcher("");

    /**
     * Private constructor as this class should not be instantiated.
     */
    private MessageParser()
    {
    }

    /**
     * The pubic API for consuming and validating quote requests from clients.
     *
     * @param message The message received from the client (e.g. '100 BUY 123').
     * @return quote request object, same as returned by QuoteRequestFactory.
     * @throws InvalidBFAMMessageException Thrown if there is a problem with the message format or the values in the message.
     */
    public static QuoteRequest parseQuoteRequest(String message) throws InvalidBFAMMessageException
    {
        try
        {
            Preconditions.checkNotNull(message);
            QUOTE_REQUEST_MESSAGE.reset(message);
            QUOTE_REQUEST_MESSAGE.find();
            Map<String, String> rawFieldMap = getRawFieldMap();
            return QuoteRequestFactory.getQuoteRequest(rawFieldMap);
        }
        catch (InvalidBFAMFieldException | IllegalStateException | NullPointerException e)
        {
            throw new InvalidBFAMMessageException("Message cannot formatted (" + message + ")", e);
        }
    }

    private static Map<String, String> getRawFieldMap()
    {
        Map<String, String> rawFieldMap = new HashMap<>();
        rawFieldMap.put(FIELD_NAME.SECURITY_ID.toString(), QUOTE_REQUEST_MESSAGE.group(1));
        rawFieldMap.put(FIELD_NAME.SIDE.toString(), QUOTE_REQUEST_MESSAGE.group(2));
        rawFieldMap.put(FIELD_NAME.QUANTITY.toString(), QUOTE_REQUEST_MESSAGE.group(3));
        return rawFieldMap;
    }
}
