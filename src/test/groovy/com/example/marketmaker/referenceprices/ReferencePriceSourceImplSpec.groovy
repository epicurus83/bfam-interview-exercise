package com.example.marketmaker.referenceprices

import com.example.marketmaker.ReferencePriceTestParams
import com.example.marketmaker.exceptions.ReferencePriceMissingException
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import java.util.concurrent.TimeUnit

class ReferencePriceSourceImplSpec extends Specification {
    @Shared
    def referencePriceSource;
    @Shared
    def listener

    def setup() {
        'Setup reference prices for security IDs 0-9'
        def referencePriceTestParams = new ReferencePriceTestParams(10)
        referencePriceSource = referencePriceTestParams.referencePriceSource
        listener = referencePriceTestParams.listener
        referencePriceSource.subscribe(listener)
    }

    @Unroll
    def 'Test SUCCESS can return reference price for security ID #securityId'() {
        when: 'Query reference price source for security ID #securityId'
        def referencePriceAct = referencePriceSource.get(securityId)

        then: 'reference price is as expected'
        referencePriceAct == referencePriceExp

        where:
        securityId || referencePriceExp
        1          || 1.1
        9          || 1.9
    }

    def 'Test throws exception if try return price for unrecognized security ID'() {
        when: 'try and get unrecognized security ID'
        referencePriceSource.get(securityId)

        then: 'ReferencePriceMissingException thrown'
        thrown(ReferencePriceMissingException)

        where:
        securityId << [-1, 10]
    }

    def 'Test if can retrieve updated price after ReferencePriceSourceListener updates'() {
        when: 'Reference price updated'
        listener.referencePriceChanged(1, BigDecimal.TEN);

        and: 'Wait for changes to be reflected'
        TimeUnit.SECONDS.sleep(1)

        then: 'Reference price source contains updated price'
        referencePriceSource.get(1) == BigDecimal.TEN
    }
}