package com.example.marketmaker.quoterequest

import com.example.marketmaker.exceptions.InvalidBFAMFieldException
import spock.lang.Specification
import spock.lang.Unroll

import static java.util.Objects.nonNull

class QuoteRequestFactorySpec extends Specification {
    def 'Test can instantiate Quote and get values'() {
        given:
        def securityIDVal = '1'
        def sideVal = 'BUY'
        def sideEnumVal = QuoteRequest.SIDE.BUY
        def quantityVal = '1';

        and:
        Map quoteRequestStringFieldMap = [:]
        quoteRequestStringFieldMap[FIELD_NAME.SECURITY_ID.toString()] = securityIDVal
        quoteRequestStringFieldMap[FIELD_NAME.SIDE.toString()] = sideVal
        quoteRequestStringFieldMap[FIELD_NAME.QUANTITY.toString()] = quantityVal

        and:
        Map quoteRequestFieldMap = [:]
        quoteRequestFieldMap[FIELD_NAME.SECURITY_ID] = new FieldString(securityIDVal)
        quoteRequestFieldMap[FIELD_NAME.SIDE] = new FieldSide(sideEnumVal)
        quoteRequestFieldMap[FIELD_NAME.QUANTITY] = new FieldFinancial(BigDecimal.ONE)

        when:
        QuoteRequest quoteRequest = QuoteRequestFactory.getQuoteRequest(quoteRequestStringFieldMap)

        then:
        quoteRequest.getField(FIELD_NAME.SECURITY_ID) == new FieldString(securityIDVal)
        quoteRequest.getField(FIELD_NAME.SECURITY_ID).getValue() == securityIDVal
        quoteRequest.getField(FIELD_NAME.SIDE) == new FieldSide(sideEnumVal)
        quoteRequest.getField(FIELD_NAME.SIDE).getValue() == sideEnumVal
        quoteRequest.getField(FIELD_NAME.QUANTITY) == new FieldFinancial(BigDecimal.ONE)
        quoteRequest.getField(FIELD_NAME.QUANTITY).getValue() == BigDecimal.ONE
        quoteRequest == new QuoteRequest(quoteRequestFieldMap)
    }

    @Unroll
    def 'Test SUCCESS if instantiate Quote with securityID = #securityIDVal, side = #sideVal, quantity = #quantityVal'() {
        given:
        Map quoteRequestFields = [:]
        quoteRequestFields[FIELD_NAME.SECURITY_ID.toString()] = securityIDVal
        quoteRequestFields[FIELD_NAME.SIDE.toString()] = sideVal
        quoteRequestFields[FIELD_NAME.QUANTITY.toString()] = quantityVal

        when:
        QuoteRequest quoteRequest = QuoteRequestFactory.getQuoteRequest(quoteRequestFields)

        then:
        nonNull(quoteRequest)

        where: 'Please note, even though a float quantity here is valid, the '
        securityIDVal | sideVal | quantityVal
        '1234567890'  | 'BUY'   | '100'
        '1234567890'  | 'SELL'  | '100'
        '0'           | 'SELL'  | '100'
    }

    @Unroll
    def 'Test FAIL if instantiate Quote with securityID = #securityIDVal, side = #sideVal, quantity = #quantityVal'() {
        given:
        Map quoteRequestFields = [:]
        quoteRequestFields[FIELD_NAME.SECURITY_ID.toString()] = securityIDVal
        quoteRequestFields[FIELD_NAME.SIDE.toString()] = sideVal
        quoteRequestFields[FIELD_NAME.QUANTITY.toString()] = quantityVal

        when:
        QuoteRequestFactory.getQuoteRequest(quoteRequestFields)

        then:
        thrown(InvalidBFAMFieldException)

        where:
        securityIDVal | sideVal | quantityVal
        'abc'         | 'BUY'   | '100'
        '1.1'         | 'BUY'   | '100'
        '-1'          | 'BUY'   | '100'
        null          | 'BUY'   | '100'
        '1'           | 'test'  | '100'
        '1'           | 'buy'   | '100'
        '1'           | null    | '100'
        '1'           | 'BUY'   | '100.15'
        '1'           | 'BUY'   | 'abc'
        '1'           | 'BUY'   | '-1'
        '1'           | 'BUY'   | null
    }

    def 'Test can convert AbstractField types to String'() {
        given:
        def stringVal = 'abc'
        def fieldString = new FieldString(stringVal)
        def sideVal = 'BUY'
        def fieldSide = new FieldSide(QuoteRequest.SIDE.valueOf(sideVal))
        def financialVal = '123'
        def fieldFinancial = new FieldFinancial(new BigDecimal(financialVal))

        expect:
        stringVal == fieldString.toString()
        sideVal == fieldSide.toString()
        financialVal == fieldFinancial.toString()
    }
}
