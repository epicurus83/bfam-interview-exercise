package com.example.marketmaker

import com.example.marketmaker.referenceprices.ReferencePriceSource
import com.example.marketmaker.referenceprices.ReferencePriceSourceListener
import com.example.marketmaker.referenceprices.ReferencePrices
import com.example.marketmaker.referenceprices.impl.PriceUpdate
import com.example.marketmaker.referenceprices.impl.ReferencePriceSourceImpl
import com.example.marketmaker.referenceprices.impl.ReferencePriceSourceListenerImpl
import com.example.marketmaker.referenceprices.impl.ReferencePricesImpl

import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.atomic.AtomicReferenceArray

class ReferencePriceTestParams {
    ReferencePriceSource referencePriceSource
    ReferencePrices referencePrices
    Queue<PriceUpdate> priceUpdates
    ReferencePriceSourceListener listener

    ReferencePriceTestParams(def numberOfSecurityIDs) {
        def initialReferencePrices = buildReferencePricesArray(numberOfSecurityIDs)
        referencePrices = ReferencePricesImpl.newInstance(initialReferencePrices)
        priceUpdates = new LinkedBlockingQueue<>()
        referencePriceSource = ReferencePriceSourceImpl.newInstance(referencePrices, priceUpdates)
        listener = new ReferencePriceSourceListenerImpl()
    }


    static AtomicReferenceArray<BigDecimal> buildReferencePricesArray(def numberOfSecurityIDs) {
        AtomicReferenceArray<BigDecimal> initialReferencePrices = new AtomicReferenceArray<>(new BigDecimal[numberOfSecurityIDs])
        (0..numberOfSecurityIDs - 1).each {
            initialReferencePrices.set(it, new BigDecimal('1.' + it))
        }
        initialReferencePrices
    }
}
