package com.example.marketmaker

import com.example.marketmaker.quotecalculationengine.QuoteCalculationEngineFactory
import spock.lang.Shared
import spock.lang.Specification

import java.util.concurrent.TimeUnit

class QuoteHandlerSpec extends Specification {
    @Shared
    QuoteHandler quoteHandler

    def setup() {
        'Setup QuoteHandler with security IDs 0-99 and 20 default quote calculation engines'
        def referencePriceTestParams = new ReferencePriceTestParams(100)
        quoteHandler = new QuoteHandler(
                QuoteCalculationEngineFactory.TYPE.DEFAULT,
                20,
                referencePriceTestParams.referencePrices,
                referencePriceTestParams.priceUpdates,
                referencePriceTestParams.listener
        )
    }

    def 'test can receive 1 quote'() {
        when: 'When submit quote to buy 123 volume of security "50"'
        quoteHandler.submitQuoteRequest('50 BUY 123')

        and: 'Wait for calculation to finish'
        TimeUnit.SECONDS.sleep(5)

        then: 'Get back quote price (This is just the reference price for security "50"'
        quoteHandler.getQuote() == '1.50'
    }
}
