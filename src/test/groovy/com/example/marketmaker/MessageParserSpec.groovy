package com.example.marketmaker

import com.example.marketmaker.exceptions.InvalidBFAMMessageException
import com.example.marketmaker.quoterequest.QuoteRequest
import com.example.marketmaker.quoterequest.QuoteRequestFactory
import spock.lang.Specification
import spock.lang.Unroll

class MessageParserSpec extends Specification {
    @Unroll
    def 'Test SUCCESS parse message #message'() {
        when: 'Parse message #message'
        QuoteRequest quoteRequestAct = MessageParser.parseQuoteRequest(message)

        then: 'Compare with expected QuoteRequest object'
        quoteRequestAct == QuoteRequestFactory.getQuoteRequest(fieldMap)

        where:
        message        || fieldMap
        '123 BUY 100'  || ['SECURITY_ID': '123', 'SIDE': 'BUY', 'QUANTITY': '100']
        '123 SELL 100' || ['SECURITY_ID': '123', 'SIDE': 'SELL', 'QUANTITY': '100']
        '0 SELL 100'   || ['SECURITY_ID': '0', 'SIDE': 'SELL', 'QUANTITY': '100']
    }

    @Unroll
    def 'Test FAIL parse message #message'() {
        when: 'Parse message #message'
        MessageParser.parseQuoteRequest(message)

        then: 'Compare with expected QuoteRequest object'
        thrown(InvalidBFAMMessageException)

        where:
        message << [
                'abc BUY 123',
                '-1 BUY 123',
                '100 buy 123',
                '100 123 123',
                '100 BUY 123.10',
                '100 BUY -123',
                '100 BUY',
                '100',
                '',
                null
        ]
    }
}
